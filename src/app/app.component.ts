import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor (private httpService: HttpClient, private sanitizer: DomSanitizer ) { }

  title = 'chat-bot';
  imagePart = true;
  chats;
  selectedQuestion;
  messages;
  finished;
  correctAnswer = false;
  allChats;
  env = 'heart';
  url;
  backgroundImgURL;
  heading;
  text;

  setImagePart() {
    this.imagePart = false;
    this.selectedQuestion = 0;
    this.chats = JSON.parse(JSON.stringify(this.allChats));
    this.messages = [{
        msg : this.chats[this.selectedQuestion].displayMsg,
        type: 0
    }];
    this.correctAnswer = false;
    this.finished = false;
  }

  ngOnInit () {
    this.backgroundImgURL = this.sanitizer.bypassSecurityTrustStyle('background-image: url("./assets/images/' + this.env + '/john-lewis-cafe.jpg")');
    if(this.env == 'stomach'){
      this.url = '../assets/Stomach_chat_bot.json';
      this.heading = 'Practice talking all things food';
      this.text = 'Answer the Guests questions, using acceptable vocabulary'
    } else {
      this.url = '../assets/ChatBot.json';
      this.heading = 'Creating emotional connections';
      this.text = 'could take practice. Your colleague, Sally, has offered to help you practice by playing the role of a Guest. Use the opportunity to show Sally a bit of ‘Heart’'
    }
    this.httpService.get(this.url).subscribe(
      data => {
        this.chats = JSON.parse(JSON.stringify(data));
        this.allChats = JSON.parse(JSON.stringify(data));
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }

  onSelectAnswer(optionIndex) {
    const question = this.chats[this.selectedQuestion];

    // this.messages = [this.messages[this.messages.length - 1]];
    this.messages.push({
      msg: question.options[optionIndex].label,
      type: 1,
      isCorrect: question.options[optionIndex].nextNode !== ''
    });

    this.messages.push({
      msg: question.options[optionIndex].hint,
      type: 0
    });

    this.chats[this.selectedQuestion].options[optionIndex].hidden = true;

    if (question.options[optionIndex].nextNode !== '') {
      // this.correctAnswer = true;
      if(this.selectedQuestion == this.chats.length - 1) {
        this.finished = true;
        this.correctAnswer=false;
      } else {
        this.moveToNextQuestion();
      }
      // setTimeout(() => {
      //   this.moveToNextQuestion();
      // }, 2000);
    }
  }

  moveToNextQuestion() {
    this.correctAnswer=false;
    if (this.selectedQuestion + 1 < this.chats.length) {
      this.selectedQuestion++;
      // this.messages = [{
      //   msg : this.chats[this.selectedQuestion].displayMsg,
      //   type: 0
      // }];
    } else {
      this.finished = true;
    }
  }

}
